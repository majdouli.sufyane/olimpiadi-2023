$(function () {
  $(".sidebar-link").click(function () {
    $(".sidebar-link").removeClass("is-active");
    $(this).addClass("is-active");
  });
});

$(window)
  .resize(function () {
    if ($(window).width() > 1090) {
      $(".sidebar").removeClass("collapse");
    } else {
      $(".sidebar").addClass("collapse");
    }
  })
  .resize();


$(function () {
  $(".logo, .logo-expand, .discover").on("click", ".discover", function (e) {
    $(".main-container").removeClass("show");
    $(".main-container").scrollTop(0);
  });
  $(".trending").on("click", "trending", function (e) {
    $(".main-container").addClass("show");
    $(".sidebar-link").removeClass("is-active");
    $(".trending").addClass("is-active");
  });
});


const scacchiera = document.getElementById("scacchiera");

for (let i = 0; i < 2; i++) {
  const item = document.createElement("div");

  item.classList.add("item");

  scacchiera.appendChild(item);
}