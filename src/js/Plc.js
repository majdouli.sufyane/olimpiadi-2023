import { stringToBool } from './helpers';
import { isMobile } from './utils';

const dataAttributePlc = '[data-plc-type]';

class Plc {
  constructor(hasReload = true, reloadTime = 1000) {
    this.file_reading_variables = 'variabili-lettura.htm';
    this.file_writing_variables = 'variabili-scrittura.htm';
    this.hasReload = hasReload;
    this.reloadTime = reloadTime;
    this.plcElements = document.querySelectorAll(dataAttributePlc);

    this.variables = {};
  }

  init() {
    // bind events
    this.eventBind();

    // inject
    this.reload();
  }

  /**
   * variables setter
   * @param {object} obj
   */
  setVariables(obj) {
    this.variables = obj;
  }

  getValues(fileReadingVariables) {
    fetch(fileReadingVariables)
      .then((response) => response.json())
      .then((json) => {
        this.setVariables(json);
      });
  }

  getValue(variable) {
    return this.variables[variable];
  }

  /**
   * bind all events with data attributes
   */
  eventBind() {
    const self = this;
    // filter the elements for output
    [...this.plcElements]
      .filter((el) => el.dataset.plcType === 'write')
      .forEach((el) => {
        switch (el.dataset.plcInputType) {
          case ('value'):
            el.addEventListener('click', (e) => {
              e.preventDefault();
              const variable = el.dataset.plcVariable;
              self.post(variable, e.target.dataset.plcInputValue);
            });
            break;
          case ('toggle'):
            el.addEventListener('click', (e) => {
              e.preventDefault();
              const variableJs = el.dataset.plcVariableJs;
              const variablePlc = el.dataset.plcVariable;
              const value = stringToBool(self.variables[variableJs]);
              self.post(variablePlc, !value);
            });
            break;
          case ('press'):
            if (isMobile()) {
              el.addEventListener('touchstart', (e) => {
                e.preventDefault();
                const variable = el.dataset.plcVariable;
                self.post(variable, true);
              });
              el.addEventListener('touchend', (e) => {
                e.preventDefault();
                const variable = el.dataset.plcVariable;
                self.post(variable, false);
              });
            } else {
              el.addEventListener('mousedown', (e) => {
                e.preventDefault();
                const variable = el.dataset.plcVariable;
                self.post(variable, true);
              });
              el.addEventListener('mouseup', (e) => {
                e.preventDefault();
                const variable = el.dataset.plcVariable;
                self.post(variable, false);
              });
            }
            break;
          default:
            console.log('default');
        }
      });
  }

  /**
   * Reload the page
   */
  reload() {
    if (this.hasReload) {
      const self = this;
      window.addEventListener('load', () => {
        setInterval(() => {
          self.getValues(self.file_reading_variables);
          self.fetchAndInject();
        }, this.reloadTime);
      });
    }
  }

  /**
   * get the data from the file with the variables and inject in dom
   */
  fetchAndInject() {
    fetch(this.file_reading_variables)
      .then((response) => response.json())
      .then((json) => {
        [...this.plcElements]
          .filter((el) => el.dataset.plcType === 'read')
          .forEach((el) => {
            if (el.dataset.plcVariableJs) {
              switch (el.dataset.plcOutputType) {
                case ('led'):
                  // FIXME: led che si accende se value a 1
                  el.classList.add('led');
                  if (json[el.dataset.plcVariableJs]) {
                    el.classList.add('is-active');
                  } else {
                    el.classList.remove('is-active');
                  }
                  break;
                default:
                  // TODO: fare con appendChild
                  el.innerHTML = json[el.dataset.plcVariableJs];
              }
            }
          });
      });
  }

  post(variable, value = null) {
    // console.log(variable + ' <- ' + value);
    const httpRequest = new XMLHttpRequest();
    httpRequest.open('POST', this.file_writing_variables);
    httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    httpRequest.send(variable + '=' + encodeURIComponent(value));
  }
}

export default Plc;
