import '../scss/app.scss';
import Plc from './Plc';
import { setVariableScrollbarWidth } from './utils';

const initApp = () => {
  // Setup
  setVariableScrollbarWidth();

  if (document.querySelector('[data-plc-type]')) {
    const plc = new Plc();
    plc.init();

    // const chartElement = document.getElementById('age_chart');
    // if (chartElement) {
    //   // Chart
    //   chartInit(chartElement);
    // }
  }
}; // initApp

/**
 * Ready with support for IE
 * https://stackoverflow.com/q/63928043/1252920
 */
if (document.readyState === 'complete' || (document.readyState !== 'loading' && !document.documentElement.doScroll)) {
  initApp();
} else {
  document.addEventListener('DOMContentLoaded', initApp);
}
