import {
  ArcElement,
  Chart,
  Legend,
  PieController,
  Tooltip,
} from 'chart.js';

export const chartInit = (chartElement) => {
  Chart.register(
    ArcElement,
    PieController,
    Legend,
    Tooltip,
  );

  const data = {
    labels: [
      '0-12',
      '12-30',
      '30-60',
      '60-100',
    ],
    datasets: [{
      label: 'Età',
      data: [400, 150, 70, 260],
      // data: [plc.getValue('eta_0_12'), plc.getValue('eta_12_30'), plc.getValue('eta_30_60'), plc.getValue('eta_60_100')],
      backgroundColor: [
        'rgb(255, 99, 132)',
        'rgb(54, 162, 235)',
        'rgb(255, 205, 86)',
        'rgb(58,184,19)',
      ],
      hoverOffset: 4,
    }],
  };

  const config = {
    type: 'pie',
    data: data,
  };

  const myChart = new Chart(
    chartElement,
    config,
  );
}
